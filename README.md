# Devcontainers Templates - Terraform

This project is a [Devcontainer](.devcontainer/) template built against both
`alpine` and `debian` images of the
[Basics template](https://gitlab.com/geekstuff.dev/devcontainers/templates/basics).

It simply adds the [terraform feature](https://gitlab.com/geekstuff.dev/devcontainers/features/terraform)
on top.

## How it's used

The .devcontainer/ configuration in this project can be used with simple entries
in your own `.devcontainer/devcontainer.json` file where you can further customize
it for example:

```json
{
    "name": "my devcontainer",
    "image": "registry.gitlab.com/geekstuff.dev/devcontainers/templates/terraform/alpine/3.17",
    "features": {
		"ghcr.io/geekstuff-dev/devcontainers-features/terraform:0.1.0": {
			"version": "latest"
		}
	}
}
```

From the example above:

- Image `alpine/3.17` can be changed to `debian/bullseye`.
- Image can also have a tag, `:latest` for the main branch, or `:vX.Y` from
  [tags in this project](https://gitlab.com/geekstuff.dev/devcontainers/templates/docker/container_registry).
- That example can also change the latest TF version to a specific one.
- See the basics template for info on how to cache VSCode extensions.
